'use strict';

require('https-proxy-agent');
/*
 * API proxy configuration.
 * This allows you to proxy HTTP request like `http.get('/api/stuff')` to another server/port.
 * This is especially useful during app development to avoid CORS issues while running a local server.
 * For more details and options, see https://github.com/angular/angular-cli#proxy-to-backend
 */
const proxyConfig = [
  {
    context: '/mbo/api/**',
    pathRewrite: {'^/mbo/api': ''},
    target: 'http://localhost:8080',
    changeOrigin: true,
    logLevel: 'debug',
    secure: false
  }
];

/*
 * Configures a corporate proxy agent for the API proxy if needed.
 */
function setupForCorporateProxy(proxyConfig) {
  if (!Array.isArray(proxyConfig)) {
    proxyConfig = [proxyConfig];
  }

  console.log('starting PROXY');
  return proxyConfig;
}

module.exports = setupForCorporateProxy(proxyConfig);
