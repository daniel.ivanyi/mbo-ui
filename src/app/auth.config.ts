import {AuthConfig} from 'angular-oauth2-oidc';

export const authConfig: AuthConfig = {
  // Url of the Identity Provider
  issuer: 'https://login-d.pss.sk:2449/prihlasenie',
  requireHttps: false,
  responseType: 'token',
  oidc: true,
  requestAccessToken: true,
  showDebugInformation: true,
  clearHashAfterLogin: false,

  // URL of the SPA to redirect the user after silent refresh
  silentRefreshRedirectUri: window.location.origin + '/silent-refresh.html',

  // The SPA's id. The SPA is registerd with this id at the auth-server
  clientId: 'kweria3',

  // set the scope for the permissions the client should request
  // The first three are defined by OIDC. The 4th is a usecase-specific one
  scope: 'openid role:user'
};
