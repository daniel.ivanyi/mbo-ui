import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GoalsRoutingModule } from './goals-routing.module';
import { GoalsComponent } from './goals.component';
import {ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../../shared/shared.module';
import {AwesomeIconsModule} from 'fox-design';
@NgModule({
  declarations: [
    GoalsComponent
  ],
  imports: [
    CommonModule,
    GoalsRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    AwesomeIconsModule
  ]
})
export class GoalsModule { }
