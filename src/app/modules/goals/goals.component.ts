import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {FormElementOption, FoxMessageType, showMessage} from 'fox-design';
import {GoalAssignmentTypeParser} from '../../core/services/parsers/goal-assignment-type-parser';
import {FindGoalCriteria, Goal, GoalAssignmentType} from 'mbo-angular-client';
import {faSearch} from '@fortawesome/free-solid-svg-icons';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import * as MboActions from '../../store/actions/mbo.actions';
import * as MboSelectors from '../../store/selectors/mbo.selectors';
import {map, tap} from 'rxjs/operators';
import {CommonTexts} from '../../data/common-texts';

@Component({
  selector: 'app-goals',
  templateUrl: './goals.component.html'
})
export class GoalsComponent implements OnInit {
  formGroup: FormGroup;
  goals$: Observable<Goal[]>;
  goals: Goal[];

  assignmentTypes: FormElementOption<GoalAssignmentType>[];
  departments: FormElementOption<string>[] = [
    {value: 'UIT', name: 'UIT'},
    {value: 'UITv', name: 'UITv'},
    {value: 'UITp', name: 'UITp'}
  ];
  groups: FormElementOption<string>[] = [
    {value: 'ALPHA', name: 'Alpha'},
    {value: 'ODIN', name: 'Odin'}
  ];
  persons: FormElementOption<string>[] = CommonTexts.persons;
  searchIcon = faSearch;

  searchCriteriaKey = 'goalsCriteria';
  formChanged$: Observable<any>;

  constructor(private fb: FormBuilder,
              private store: Store) {
    this.formGroup = this.fb.group({
      year: [null, Validators.required],
      assignmentType: [null, Validators.required],
      assignTo: [null]
    });
  }

  ngOnInit(): void {
    this.initFormGroupChangeListener();
    this.initAssignmentTypes();
    this.readSearchCriteria();
  }

  find(): void {
    console.log('find');
    this.formGroup.markAllAsTouched();
    if (this.formGroup.get('assignmentType').value !== GoalAssignmentType.Company
      && !this.formGroup.get('assignTo').value) {
      this.store.dispatch(showMessage({
        messageType: FoxMessageType.ERROR,
        text: 'Vyplňte všetky vstupné údaje.'
      }));
      return;
    }
    if (!this.formGroup.valid) {
      console.log('formGroup is not valid', this.formGroup);
      return;
    }

    this.store.dispatch(MboActions.findGoal({criteria: this.formGroup.getRawValue()}));
    this.initGoalsListener();
    this.saveCriteria();
  }

  private initFormGroupChangeListener(): void {
    this.formChanged$ = this.formGroup.get('assignmentType').valueChanges.pipe(
      tap((d) => {
        const assignToFormControl = this.formGroup.get('assignTo') as FormControl;
        assignToFormControl.setValue(null);
      })
    );
  }

  private initAssignmentTypes(): void {
    this.assignmentTypes = GoalAssignmentTypeParser.options
      .map((d) => {
        return {
          value: d.key,
          name: d.text
        } as FormElementOption<GoalAssignmentType>;
      });
  }

  private initGoalsListener(): void {
    this.goals$ = this.store.select(MboSelectors.goals).pipe(
      map((d) => this.goals = [...d]));
  }

  private readSearchCriteria(): void {
    const criteria = JSON.parse(sessionStorage.getItem(this.searchCriteriaKey));
    if (criteria) {
      this.formGroup.patchValue(criteria);
      this.find();
    }
  }

  private saveCriteria(): void {
    const criteria = this.formGroup.getRawValue() as FindGoalCriteria;
    sessionStorage.setItem(this.searchCriteriaKey, JSON.stringify(criteria));
  }
}
