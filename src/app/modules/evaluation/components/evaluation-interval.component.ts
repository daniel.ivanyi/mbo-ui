import {Component, Input} from '@angular/core';
import {EvaluationInterval} from 'mbo-angular-client';

@Component({
  selector: 'app-evaluation-interval',
  templateUrl: './evaluation-interval.component.html'
})
export class EvaluationIntervalComponent {

  @Input() interval!: EvaluationInterval;

}
