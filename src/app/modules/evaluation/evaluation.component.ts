import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import * as EvaluationActions from '../../store/actions/evaluation.ations';
import * as EvaluationSelectors from '../../store/selectors/evaluation.selectors';
import {EvaluationInterval} from 'mbo-angular-client';

@Component({
  templateUrl: './evaluation.component.html'
})
export class EvaluationComponent implements OnInit {

  loading$: Observable<boolean>;
  evaluations$: Observable<EvaluationInterval[]>;

  constructor(private store: Store) {
  }

  ngOnInit(): void {
    this.store.dispatch(EvaluationActions.load());

    this.evaluations$ = this.store.select(EvaluationSelectors.intervals);
    this.loading$ = this.store.select(EvaluationSelectors.loading);
  }

}
