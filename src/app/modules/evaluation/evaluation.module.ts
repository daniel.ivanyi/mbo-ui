import {NgModule} from '@angular/core';
import {CommonModule, DecimalPipe} from '@angular/common';

import {EvaluationRoutingModule} from './evaluation-routing.module';
import {EvaluationComponent} from './evaluation.component';
import {SlovakNumberPipe} from 'fox-design';
import {SharedModule} from '../../shared/shared.module';
import {EvaluationIntervalComponent} from './components/evaluation-interval.component';


@NgModule({
  declarations: [
    EvaluationComponent,
    EvaluationIntervalComponent
  ],
  imports: [
    CommonModule,
    EvaluationRoutingModule,
    SharedModule
  ],
  providers: [
    SlovakNumberPipe,
    DecimalPipe
  ]
})
export class EvaluationModule { }
