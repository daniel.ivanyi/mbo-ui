import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MboComponent} from './mbo.component';

const routes: Routes = [
  {path: '', component: MboComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MboRoutingModule { }
