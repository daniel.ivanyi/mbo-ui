import {NgModule} from '@angular/core';
import {MboComponent} from './mbo.component';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../../shared/shared.module';
import {MboRoutingModule} from './mbo-routing.module';
import {UserService} from '../../core/services/user.service';
import {ReactiveFormsModule} from '@angular/forms';
import {AwesomeIconsModule} from 'fox-design';
import {EducationComponent} from './education/education.component';

@NgModule({
  declarations: [
    MboComponent,
    EducationComponent
  ],
    imports: [
        CommonModule,
        SharedModule,
        MboRoutingModule,
        ReactiveFormsModule,
        AwesomeIconsModule
    ],
  providers: [
    UserService
  ]
})
export class MboModule {

}
