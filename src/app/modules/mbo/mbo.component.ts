import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {FindGoalCriteria, Goal, GoalAssignmentType, Mbo, Plan, Signature} from 'mbo-angular-client';
import {DateTime} from 'luxon';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {faCog, faMinus, faPlus, faSave, faSearch} from '@fortawesome/free-solid-svg-icons';
import {DialogService, FormElementOption, User} from 'fox-design';
import {UserService} from '../../core/services/user.service';
import {RoleService} from '../../core/services/role.service';
import {map, tap} from 'rxjs/operators';
import * as MboActions from '../../store/actions/mbo.actions';
import * as MboSelectors from '../../store/selectors/mbo.selectors';
import {CommonTexts} from '../../data/common-texts';
import {Education} from '../../data/education';
import {EducationComponent} from './education/education.component';
import {EducationType} from '../../data/education-type';

@Component({
  selector: 'app-mbo',
  templateUrl: './mbo.component.html'
})
export class MboComponent implements OnInit {

  mboFormGroup!: FormGroup;
  definitionFormGroup: FormGroup;
  searchFormGroup: FormGroup;

  completedOn = 0;

  mbo$: Observable<Mbo>;
  mbo: Mbo;
  mbos$: Observable<Mbo[]>;
  mbos: Mbo[];
  company$: Observable<Plan>;
  performance$: Observable<Plan>;
  competency$: Observable<Plan>;
  user$: Observable<User>;
  user: User;
  isManager$: Observable<boolean>;
  isManager: boolean;
  educations: Education[] = [
    {type: EducationType.PROFI, name: 'Best Java Features'},
    {type: EducationType.PROFI, name: 'Angular For Gurus'},
    {type: EducationType.SOFT, name: 'Ako v debate nezabit'},
    {type: EducationType.HARD, name: 'Ako urobiť ženu šťastnú'},
    {type: EducationType.LANGUAGE, name: 'Angličtina'},
    {type: EducationType.PROJECT, name: 'Scrum For Dummies'}
  ];

  currentYear = DateTime.now().year;

  iconCog = faCog;
  iconSave = faSave;
  iconPlus = faPlus;
  iconMinus = faMinus;

  coefficientTargetRewards: FormElementOption<number>[] = [
    {value: 1, name: '1'},
    {value: 2, name: '2'},
    {value: 3, name: '3'}
  ];
  typeOfWeights: FormElementOption<number>[] = [
    {value: 1, name: '1'},
    {value: 2, name: '2'},
    {value: 3, name: '3'}
  ];

  persons: FormElementOption<string>[] = CommonTexts.persons;
  goalMap: Map<GoalAssignmentType, Goal[]> = new Map<GoalAssignmentType, Goal[]>();
  goalVisibilityMap: Map<GoalAssignmentType, boolean> = new Map<GoalAssignmentType, boolean>();
  searchIcon = faSearch;
  private searchCriteriaKey = 'mboCriteria';
  educationDialog$: Observable<any>;

  constructor(private store: Store,
              private fb: FormBuilder,
              private userService: UserService,
              private roleService: RoleService,
              private dialogService: DialogService) {
    this.searchFormGroup = this.fb.group({
      year: [null, Validators.required],
      username: [null, Validators.required]
    });
    this.definitionFormGroup = this.fb.group({
      coefficientTargetReward: [2, Validators.required],
      ratioCompanyGoalsToTargetReward: [null, Validators.required],
      typeOfWeight: [1, Validators.required],
    });
    this.mboFormGroup = this.fb.group({
      employee: [null],
      manager: [null],
      comment: [null]
    });
  }

  ngOnInit(): void {
    this.initUserListener();

    this.initMboListener();
    this.initIsManagerListener();
    this.readSearchCriteria();
    this.goalMap = new Map<GoalAssignmentType, Goal[]>();
    this.goalVisibilityMap = new Map<GoalAssignmentType, boolean>();
  }

  find(): void {
    console.log('find');
    this.searchFormGroup.markAllAsTouched();
    if (!this.searchFormGroup.valid) {
      console.log('searchFormGroup is not valid', this.searchFormGroup);
      return;
    }

    this.store.dispatch(MboActions.findMbo({criteria: this.searchFormGroup.getRawValue()}));
    this.initMboListListener();
    this.saveCriteria();
  }

  create(): void {
    console.log('create mbo');
    this.store.dispatch(MboActions.createMbo({username: this.user.login}));
  }

  save(): void {
    console.log('save');
    const finalEvaluation = this.mboFormGroup.getRawValue();
    const tempMbo = {
      ...this.mbo,
      finalEvaluation
    } as Mbo;
    this.store.dispatch(MboActions.saveMbo({mbo: tempMbo}));
  }

  employeeConfirmedSetting(signature: Signature): void {
    const mboTemp = {...this.mbo};
    mboTemp.signatures = {
      ...this.mbo?.signatures,
      employeeMboSetting: signature
    };
    this.store.dispatch(MboActions.saveMbo({mbo: mboTemp}));
  }

  managerConfirmedSetting(signature: Signature): void {
    console.log('managerConfirmedSetting', this.mbo);
    const mboTemp = {...this.mbo};
    console.log('managerConfirmedSetting', mboTemp);
    mboTemp.signatures = {
      ...this.mbo?.signatures,
      managerMboSetting: signature
    };
    this.store.dispatch(MboActions.saveMbo({mbo: mboTemp}));
  }

  employeeConfirmedSemiEvaluation(signature: Signature): void {
    const mboTemp = {...this.mbo};
    mboTemp.signatures = {
      ...this.mbo?.signatures,
      employeeSemiAnnualMboEvaluation: signature
    };
    this.store.dispatch(MboActions.saveMbo({mbo: mboTemp}));
  }

  managerConfirmedSemiEvaluation(signature: Signature): void {
    const mboTemp = {...this.mbo};
    mboTemp.signatures = {
      ...this.mbo?.signatures,
      managerSemiAnnualMboEvaluation: signature
    };
    this.store.dispatch(MboActions.saveMbo({mbo: mboTemp}));
  }

  employeeConfirmedFinalEvaluation(signature: Signature): void {
    const mboTemp = {...this.mbo};
    mboTemp.signatures = {
      ...this.mbo?.signatures,
      employeeFinalMboEvaluation: signature
    };
    this.store.dispatch(MboActions.saveMbo({mbo: mboTemp}));
  }

  managerConfirmedFinalEvaluation(signature: Signature): void {
    const mboTemp = {...this.mbo};
    mboTemp.signatures = {
      ...this.mbo?.signatures,
      managerFinalMboEvaluation: signature
    };
    this.store.dispatch(MboActions.saveMbo({mbo: mboTemp}));
  }

  employeeConfirmedEducation(signature: Signature): void {
    const mboTemp = {...this.mbo};
    mboTemp.signatures = {
      ...this.mbo?.signatures,
      employeeEducation: signature
    };
    this.store.dispatch(MboActions.saveMbo({mbo: mboTemp}));
  }

  managerConfirmedEducation(signature: Signature): void {
    const mboTemp = {...this.mbo};
    mboTemp.signatures = {
      ...this.mbo?.signatures,
      managerEducation: signature
    };
    this.store.dispatch(MboActions.saveMbo({mbo: mboTemp}));
  }

  changeVisibilityOfPlan(key: GoalAssignmentType): void {
    this.goalVisibilityMap.set(key, !this.goalVisibilityMap.get(key));
  }

  openAddEducation(): void {
    const modalRef = this.dialogService.open(EducationComponent, {
      formGroup: this.fb.group({
        type: [null, Validators.required],
        name: [null, Validators.required]
      }),
      store: this.store
    });
    this.educationDialog$ = modalRef.closed.pipe(
      tap(d => {
        if (d) {
          this.addEducation(d);
        }
      }));
  }

  addEducation(education: Education): void {
    this.educations.push(education);
  }

  private initIsManagerListener(): void {
    this.isManager$ = this.roleService.isManager().pipe(
      tap((d) => {
        this.isManager = d;

        if (!this.isManager) {
          setTimeout(() => {
            const usernameControl = this.searchFormGroup.get('username') as FormControl;
            usernameControl.setValue(this.user.login);
          });
        }
      })
    );
  }

  private initMboListener(): void {
    this.mbo$ = this.store.select(MboSelectors.mbo).pipe(
      map((d) => {
        if (!d) {
          return null;
        }
        this.mbo = {...d};
        console.log('mbo', this.mbo);
        this.mboFormGroup.patchValue(this.mbo.finalEvaluation);
        this.setGoalMap(this.mbo?.goals);

        return d;
      })
    );
  }

  private initMboListListener(): void {
    this.mbos$ = this.store.select(MboSelectors.mbos).pipe(
      map((d) => {
        console.log('initMboListListener', d);
        this.mbos = [...d];
        if (this.mbos && this.mbos.length > 0) {
          this.mbo = this.mbos[0];
          this.mboFormGroup.patchValue(this.mbo.finalEvaluation);
          this.setGoalMap(this.mbo.goals);
        }

        return this.mbos;
      })
    );
  }

  private setGoalMap(goals: Goal[]): void {
    goals?.forEach((g) => {
      if (!this.goalMap.get(g.assignmentType)) {
        this.goalMap.set(g.assignmentType, []);
      }
      this.goalMap.get(g.assignmentType).push(g);

      if (!this.goalVisibilityMap.get(g.assignmentType)) {
        this.goalVisibilityMap.set(g.assignmentType, false);
      }
    });
    console.log('goalMap', this.goalMap);
  }

  private initUserListener(): void {
    this.user$ = this.userService.user$.pipe(
      tap((d) => {
        if (!d) {
          return;
        }
        this.user = d;
        console.log('user loaded', this.user);
      })
    );
  }

  private readSearchCriteria(): void {
    const criteria = JSON.parse(sessionStorage.getItem(this.searchCriteriaKey));
    if (criteria && criteria.year && criteria.username) {
      console.log('criteria', criteria);
      this.searchFormGroup.patchValue(criteria);
      this.find();
    }
  }

  private saveCriteria(): void {
    const criteria = this.searchFormGroup.getRawValue() as FindGoalCriteria;
    sessionStorage.setItem(this.searchCriteriaKey, JSON.stringify(criteria));
  }
}
