import {Component, Input, OnInit} from '@angular/core';
import {AbstractDialogComponent, FormElementOption} from 'fox-design';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Goal} from 'mbo-angular-client';
import {EducationType} from '../../../data/education-type';
import {EducationTypeParser} from '../../../core/services/parsers/education-type-parser';
import {FormGroup} from '@angular/forms';

@Component({
  templateUrl: 'education.component.html'
})
export class EducationComponent extends AbstractDialogComponent implements OnInit {

  @Input() formGroup!: FormGroup;

  types: FormElementOption<EducationType>[];

  constructor(activeModal: NgbActiveModal) {
    super(activeModal);
  }

  ngOnInit(): void {
    this.types = EducationTypeParser.texts.map((d) => {
      return {
        value: d.key,
        name: d.text
      } as FormElementOption<EducationType>;
    });
  }

  dismiss(): void {
    this.activeModal.dismiss();
  }

  onSubmit(): void {
    if (this.formGroup.valid) {
      const education = {...this.formGroup.getRawValue()} as Goal;
      this.activeModal.close(education);
    } else {
      this.formGroup.markAllAsTouched();
      console.log('goal formGroup is not valid', this.formGroup);
    }
  }
}
