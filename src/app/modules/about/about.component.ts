import {Component} from '@angular/core';
import {Store} from '@ngrx/store';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html'
})
export class AboutComponent {

  constructor(private store: Store) {
  }
}
