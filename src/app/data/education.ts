import {EducationType} from './education-type';

export interface Education {
  type: EducationType;
  name: string;
}
