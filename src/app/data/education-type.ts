export enum EducationType {
  PROFI = 'PROFI',
  SOFT = 'SOFT',
  HARD = 'HARD',
  LANGUAGE = 'LANGUAGE',
  PROJECT = 'PROJECT'
}
