import {DateTime} from 'luxon';
import {FormElementOption} from 'fox-design';

export class CommonTexts {

  public static get now(): string {
    return DateTime.now().toISO({includeOffset: false});
  }

  public static get persons(): FormElementOption<string>[] {
    return [
      {value: '118881497', name: 'Dano Ivanyi'},
      {value: '118881688', name: 'Maťo Šubrt'},
      {value: '118881767', name: 'Tomino Benčo'},
      {value: '118881841', name: 'Maco Illiť'},
    ];
  }
}
