import {EvaluationInterval} from 'mbo-angular-client';

export class EvaluationIntervalProvider {

  static getIntervals(): EvaluationInterval[] {
    return [
      {
        start: 0.00,
        end: 0.7499,
        mark: 1,
        successOfGoal: 0.00,
        background: 'bg-light-dark'
      },
      {
        start: 0.7500,
        end: 0.8499,
        mark: 2,
        successOfGoal: 0.80,
        background: 'bg-light-info'
      },
      {
        start: 0.8500,
        end: 0.9499,
        mark: 3,
        successOfGoal: 0.90,
        background: 'bg-light-primary'
      },
      {
        start: 0.9500,
        end: 1.0499,
        mark: 4,
        successOfGoal: 1.00,
        background: 'bg-light-danger'
      },
      {
        start: 1.0500,
        end: 1.1499,
        mark: 5,
        successOfGoal: 1.10,
        background: 'bg-light-warning'
      },
      {
        start: 1.1500,
        mark: 6,
        successOfGoal: 1.20,
        background: 'bg-light-success'
      }
    ];
  }
}
