import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {catchError, map, switchMap, withLatestFrom} from 'rxjs/operators';
import {FoxMessageType, selectUser, showMessage} from 'fox-design';
import {FindGoalCriteria, Goal, GoalService, MboService, PlanService} from 'mbo-angular-client';
import {Store} from '@ngrx/store';
import {Observable, of} from 'rxjs';
import * as EvaluationActions from '../actions/evaluation.ations';
import * as MboActions from '../actions/mbo.actions';
import * as MboSelectors from '../selectors/mbo.selectors';
import {GoalAssignmentTypeParser} from '@service/parsers/goal-assignment-type-parser';

@Injectable()
export class MboEffects {

  constructor(private actions$: Actions,
              private store: Store,
              private goalApi: GoalService,
              private planApi: PlanService,
              private mboApi: MboService) {
  }

  errors$ = createEffect(() => this.actions$.pipe(
    ofType(
      EvaluationActions.loadFailed,
      MboActions.initGoalsFailed,
      MboActions.saveGoalFailed,
      MboActions.removeGoalFailed,
      MboActions.findGoalsFailed,
      MboActions.lockGoalFailed,
      MboActions.unLockGoalFailed,
      MboActions.createMboFailed,
      MboActions.findMboFailed,
      MboActions.saveMboFailed
    ),
    map((action) => {
      return showMessage({
        messageType: FoxMessageType.ERROR,
        text: JSON.stringify(action.error)
      });
    })
  ));

  initPlan$ = createEffect(() => this.actions$.pipe(
    ofType(MboActions.initGoal),
    withLatestFrom(this.getGoalCriteria()),
    switchMap(([_, criteria]) => {
      return this.planApi.initPlan(criteria).pipe(
        map((d) => {
          console.log('plan initiated', d);
          return MboActions.initGoalsSuccess({goals: d.goals});
        }),
        catchError(error => of(MboActions.initGoalsFailed({error})))
      );
    })
  ));

  initPlanSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(MboActions.initGoalsSuccess),
    withLatestFrom(this.getGoalCriteria()),
    map(([_, criteria]) => {
      const text = `${GoalAssignmentTypeParser.getTitle(criteria.assignmentType)} ciele na rok ${criteria.year} boli inicializovane podla predchádzajúceho roka`;
      return showMessage({
        messageType: FoxMessageType.SUCCESS,
        text
      });
    })
  ));

  findPlan$ = createEffect(() => this.actions$.pipe(
    ofType(MboActions.findGoal,
      MboActions.saveGoalSuccess,
      MboActions.removeGoalSuccess,
      MboActions.lockGoalSuccess,
      MboActions.unLockGoalSuccess),
    withLatestFrom(this.getGoalCriteria()),
    switchMap(([_, criteria]) => {
      return this.findGoals(criteria).pipe(
        map((d) => {
          return MboActions.findGoalsSuccess({goals: d});
        }),
        catchError(error => of(MboActions.findGoalsFailed({error})))
      );
    })
  ));

  savePlan$ = createEffect(() => this.actions$.pipe(
    ofType(MboActions.saveGoal),
    switchMap(({goal}) => {
      return this.save(goal).pipe(
        map((d) => {
          return MboActions.saveGoalSuccess();
        }),
        catchError(error => of(MboActions.saveGoalFailed({error})))
      );
    })
  ));

  lockGoal$ = createEffect(() => this.actions$.pipe(
    ofType(MboActions.lockGoal),
    switchMap(({goal}) => {
      return this.save(goal).pipe(
        map((d) => {
          return MboActions.lockGoalSuccess();
        }),
        catchError(error => of(MboActions.lockGoalFailed({error})))
      );
    })
  ));

  lockGoalSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(MboActions.lockGoalSuccess),
    map((action) => {
      return showMessage({
        messageType: FoxMessageType.SUCCESS,
        text: 'Cieľ bol zamknutý'
      });
    })
  ));

  unLockGoal$ = createEffect(() => this.actions$.pipe(
    ofType(MboActions.unLockGoal),
    switchMap(({goal}) => {
      return this.save(goal).pipe(
        map((d) => {
          return MboActions.unLockGoalSuccess();
        }),
        catchError(error => of(MboActions.unLockGoalFailed({error})))
      );
    })
  ));

  unLockGoalSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(MboActions.unLockGoalSuccess),
    map(() => {
      return showMessage({
        messageType: FoxMessageType.SUCCESS,
        text: 'Cieľ bol odomknutý'
      });
    })
  ));


  removeGoal$ = createEffect(() => this.actions$.pipe(
    ofType(MboActions.removeGoal),
    switchMap(({goalId}) => {
      return this.remove(goalId).pipe(
        map((d) => {
          if ('DELETED' === d) {
            return MboActions.removeGoalSuccess();
          } else {
            return MboActions.removeGoalFailed({error: d});
          }
        })
      );
    })
  ));

  removeGoalSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(MboActions.removeGoalSuccess),
    map(() => {
      return showMessage({
        messageType: FoxMessageType.SUCCESS,
        text: 'Cieľ bol odstránený'
      });
    })
  ));

  createMbo$ = createEffect(() => this.actions$.pipe(
    ofType(MboActions.createMbo),
    switchMap(({username}) => {
      return this.mboApi.createMbo({username}).pipe(
        map((d) => {
          return MboActions.createMboSuccess({mbo: d});
        }),
        catchError(error => of(MboActions.createMboFailed({error})))
      );
    })
  ));

  createMboSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(MboActions.createMboSuccess),
    withLatestFrom(this.store.select(selectUser)),
    map(([{mbo}, user]) => {
      return showMessage({
        messageType: FoxMessageType.INFO,
        text: `MbO pre zamestnanca ${user.name} na rok ${mbo.year} bol vytvorený`
      });
    })
  ));

  findMbo$ = createEffect(() => this.actions$.pipe(
    ofType(MboActions.findMbo),
    switchMap(({criteria}) => {
      return this.mboApi.findMbos(criteria).pipe(
        map((d) => {
          return MboActions.findMboSuccess({mbos: d});
        }),
        catchError(error => of(MboActions.findMboFailed({error})))
      );
    })
  ));

  saveMbo$ = createEffect(() => this.actions$.pipe(
    ofType(MboActions.saveMbo),
    switchMap(({mbo}) => {
      console.log('save mbo', mbo);
      return this.mboApi.updateMbo(mbo.id, mbo).pipe(
        map((d) => {
          return MboActions.saveMboSuccess({mbo: d});
        }),
        catchError(error => of(MboActions.saveMboFailed({error})))
      );
    })
  ));

  private getGoalCriteria(): Observable<FindGoalCriteria> {
    return this.store.select(MboSelectors.goalCriteria);
  }

  private findGoals(criteria: FindGoalCriteria): Observable<Goal[]> {
    return this.goalApi.findGoals(criteria);
  }

  /**
   * Prida alebo updatne existujuci zaznam.
   *
   * @param goal ciel na ulozenie
   * @private ulozeny ciel
   */
  private save(goal: Goal): Observable<Goal> {
    if (goal.id) {
      return this.goalApi.updateGoal(goal.id, goal);
    }

    return this.goalApi.createGoal(goal);
  }

  private remove(goalId: string): Observable<string | any> {
    return this.goalApi.removeGoal(goalId).pipe(
      map(() => 'DELETED'),
      catchError(error => {
        console.error(`cannot remove goal[id=${goalId}]`, error);
        showMessage({
          messageType: FoxMessageType.ERROR,
          text: JSON.stringify(error)
        });
        return error;
      })
    );
  }
}
