import {MboEffects} from '@app/store/effects/mbo.effects';
import {EvaluationEffects} from '@app/store/effects/evaluation.effects';

export const effects: any[] = [MboEffects, EvaluationEffects];
