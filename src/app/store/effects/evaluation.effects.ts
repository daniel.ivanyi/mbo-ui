import {Actions, createEffect, ofType} from '@ngrx/effects';
import * as EvaluationActions from '../actions/evaluation.ations';
import {switchMap} from 'rxjs/operators';
import {of} from 'rxjs';
import {Injectable} from '@angular/core';
import {EvaluationInterval} from 'mbo-angular-client';
import {EvaluationIntervalProvider} from '@data/evaluation-interval.provider';

@Injectable()
export class EvaluationEffects {

  constructor(private actions$: Actions) {
  }

  load$ = createEffect(() => this.actions$.pipe(
    ofType(EvaluationActions.load),
    switchMap(() => {
      return of(EvaluationActions.loadSuccess({intervals: EvaluationIntervalProvider.getIntervals()}));
    })
  ));
}
