import {ActionReducerMap} from '@ngrx/store';
import * as fromMbo from './mbo.reducer';
import * as fromEvaluation from './evaluation.reducer';
import {MessageDataState, messageReducer} from 'fox-design';

export interface CoreState {
  app: fromMbo.MboState;
  evaluation: fromEvaluation.EvaluationState;
  message: MessageDataState;
}

export const reducers: ActionReducerMap<CoreState> = {
  app: fromMbo.reducer,
  evaluation: fromEvaluation.reducer,
  message: messageReducer
};
