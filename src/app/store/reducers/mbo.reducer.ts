import {createReducer, on} from '@ngrx/store';
import {FindGoalCriteria, FindMboCriteria, Goal, Mbo} from 'mbo-angular-client';
import * as MboActions from '../actions/mbo.actions';

export interface MboState {

  readonly loading: boolean;
  readonly goalCriteria: FindGoalCriteria;
  readonly goals: Goal[];
  readonly mboCriteria: FindMboCriteria,
  readonly mbo: Mbo;
  readonly mbos: Mbo[];
}

export const initialState: MboState = {
  loading: false,
  goalCriteria: null,
  goals: [],
  mboCriteria: null,
  mbo: null,
  mbos: []
};

export const reducer = createReducer(
  initialState,
  on(MboActions.initGoal,
    MboActions.saveGoal,
    MboActions.removeGoal,
    MboActions.findGoal,
    MboActions.lockGoal,
    MboActions.unLockGoal,
    MboActions.createMbo,
    MboActions.findMbo,
    MboActions.saveMbo, (state) => ({
      ...state,
      loading: true
    })),
  on(MboActions.saveGoalSuccess,
    MboActions.saveGoalFailed,
    MboActions.removeGoalSuccess,
    MboActions.removeGoalFailed,
    MboActions.findGoalsSuccess,
    MboActions.findGoalsFailed,
    MboActions.lockGoalSuccess,
    MboActions.lockGoalFailed,
    MboActions.unLockGoalSuccess,
    MboActions.unLockGoalFailed,
    MboActions.createMboSuccess,
    MboActions.createMboFailed,
    MboActions.findMboSuccess,
    MboActions.findMboFailed,
    MboActions.saveMboSuccess,
    MboActions.saveMboFailed, (state) => ({
      ...state,
      loading: false
    })),
  on(MboActions.findGoal, (state, {criteria}) => ({
    ...state,
    goalCriteria: criteria
  })),
  on(MboActions.findGoalsSuccess,
    MboActions.initGoalsSuccess, (state, {goals}) => ({
      ...state,
      goals
    })),
  on(MboActions.createMboSuccess, (state, {mbo}) => ({
    ...state,
    mbo
  })),
  on(MboActions.findMbo, (state, {criteria}) => ({
    ...state,
    mboCriteria: criteria
  })),
  on(MboActions.findMboSuccess, (state, {mbos}) => ({
    ...state,
    mbos
  })),
  on(MboActions.saveMboSuccess, (state, {mbo}) => ({
    ...state,
    mbo
  }))
);
