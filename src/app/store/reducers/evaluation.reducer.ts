import {createReducer, on} from '@ngrx/store';
import * as EvaluationActions from '../actions/evaluation.ations';
import {EvaluationInterval} from 'mbo-angular-client';

export interface EvaluationState {
  readonly loading: boolean;
  readonly intervals: EvaluationInterval[];
}

export const initialState: EvaluationState = {
  intervals: [],
  loading: false
};

export const reducer = createReducer(
  initialState,
  on(EvaluationActions.load, (state) => ({
    ...state,
    loading: true
  })),
  on(EvaluationActions.loadSuccess, (state, {intervals}) => ({
    ...state,
    loading: false,
    intervals
  })),
  on(EvaluationActions.loadFailed, (state) => ({
    ...state,
    loading: false
  }))
);
