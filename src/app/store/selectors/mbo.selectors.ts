import {createSelector} from '@ngrx/store';
import {CoreState} from '../reducers';
import {MboState} from '../reducers/mbo.reducer';

export const selectorState = (state: CoreState) => state.app;

export const loading = createSelector(selectorState, (state: MboState) => state.loading);

export const goalCriteria = createSelector(selectorState, (state: MboState) => state.goalCriteria);
export const goals = createSelector(selectorState, (state: MboState) => state.goals);

export const mboCriteria = createSelector(selectorState, (state: MboState) => state.mboCriteria);
export const mbo = createSelector(selectorState, (state: MboState) => state.mbo);
export const mbos = createSelector(selectorState, (state: MboState) => state.mbos);
