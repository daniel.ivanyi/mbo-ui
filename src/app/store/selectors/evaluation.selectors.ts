import {CoreState} from '../reducers';
import {createSelector} from '@ngrx/store';

export const selectorsEvaluationState = (state: CoreState) => state.evaluation;

export const intervals = createSelector(selectorsEvaluationState, (state) => state.intervals);
export const loading = createSelector(selectorsEvaluationState, (state) => state.loading);
