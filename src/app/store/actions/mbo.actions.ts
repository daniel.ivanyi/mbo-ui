import {createAction, props} from '@ngrx/store';
import {FindGoalCriteria, FindMboCriteria, Goal, Mbo} from 'mbo-angular-client';

// EMPTY ACTION
export const doNothing = createAction(
  '[MBO] Do Nothing'
);

// INIT AS PREVIOUS
export const initGoal = createAction(
  '[GOAL] Init'
);
export const initGoalsSuccess = createAction(
  '[GOAL] Init Success',
  props<{ goals: Goal[] }>()
);
export const initGoalsFailed = createAction(
  '[GOAL] Init Failed',
  props<{ error: any }>()
);


// FIND GOALS
export const findGoal = createAction(
  '[GOAL] Find',
  props<{ criteria: FindGoalCriteria }>()
);
export const findGoalsSuccess = createAction(
  '[GOAL] Find Success',
  props<{ goals: Goal[] }>()
);
export const findGoalsFailed = createAction(
  '[GOAL] Find Failed',
  props<{ error: any }>()
);

// SAVE GOAL
export const saveGoal = createAction(
  '[GOAL] Save',
  props<{ goal: Goal }>()
);
export const saveGoalSuccess = createAction(
  '[GOAL] Save Success'
);
export const saveGoalFailed = createAction(
  '[GOAL] Save Failed',
  props<{ error: any }>()
);

// LOCK
export const lockGoal = createAction(
  '[GOAL] Lock',
  props<{ goal: Goal }>()
);
export const lockGoalSuccess = createAction(
  '[GOAL] Lock Success'
);
export const lockGoalFailed = createAction(
  '[GOAL] Lock Failed',
  props<{ error: any }>()
);

// UNLOCK
export const unLockGoal = createAction(
  '[GOAL] Unlock',
  props<{ goal: Goal }>()
);
export const unLockGoalSuccess = createAction(
  '[GOAL] Unlock Success'
);
export const unLockGoalFailed = createAction(
  '[GOAL] Unlock Failed',
  props<{ error: any }>()
);

// REMOVE
export const removeGoal = createAction(
  '[GOAL] Remove',
  props<{ goalId: string }>()
);
export const removeGoalSuccess = createAction(
  '[GOAL] Remove Success'
);
export const removeGoalFailed = createAction(
  '[GOAL] Remove Failed',
  props<{ error: any }>()
);

// MBO CREATE
export const createMbo = createAction(
  '[MBO] Create',
  props<{ username: string }>()
);
export const createMboSuccess = createAction(
  '[MBO] Create Success',
  props<{ mbo: Mbo }>()
);
export const createMboFailed = createAction(
  '[MBO] Create Failed',
  props<{ error: any }>()
);
// MBO FIND
export const findMbo = createAction(
  '[MBO] Find',
  props<{ criteria: FindMboCriteria }>()
);
export const findMboSuccess = createAction(
  '[MBO] Find Success',
  props<{ mbos: Mbo[] }>()
);
export const findMboFailed = createAction(
  '[MBO] Find Failed',
  props<{ error: any }>()
);
// MBO Save
export const saveMbo = createAction(
  '[MBO] Save',
  props<{ mbo: Mbo }>()
);
export const saveMboSuccess = createAction(
  '[MBO] Save Success',
  props<{ mbo: Mbo }>()
);
export const saveMboFailed = createAction(
  '[MBO] Save Failed',
  props<{ error: any }>()
);

