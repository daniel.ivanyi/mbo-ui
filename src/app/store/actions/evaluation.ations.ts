import {createAction, props} from '@ngrx/store';
import {EvaluationInterval} from 'mbo-angular-client';

export const load = createAction(
  '[EVALUATION] Load'
);
export const loadSuccess = createAction(
  '[EVALUATION] Load Success',
  props<{ intervals: EvaluationInterval[] }>()
);
export const loadFailed = createAction(
  '[EVALUATION] Load Failed',
  props<{ error: any }>()
);
