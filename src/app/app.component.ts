import {Component, Inject, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Store} from '@ngrx/store';
import {CodeBookService, environment, loadConfigEnvironment, selectUser, User} from 'fox-design';
import {Observable, Subject} from 'rxjs';
import {APP_BASE_HREF} from '@angular/common';
import {Actions} from '@ngrx/effects';
import {WINDOW} from '@core/core.module';
import {RoleService} from '@service/role.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'mbo-ui';
  loggedUser$: Observable<User>;
  private onDestroy$ = new Subject();

  constructor(
    private store: Store,
    private codeBookService: CodeBookService,
    private roleService: RoleService,
    private actions$: Actions,
    @Inject(APP_BASE_HREF) private baseHref: string,
    @Inject(WINDOW) private window: Window
  ) {
    console.log('BaseHref is: ' + this.baseHref);
  }

  ngOnInit(): void {
    this.store.dispatch(loadConfigEnvironment({environment}));
    this.loggedUser$ = this.store.select(selectUser);
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
