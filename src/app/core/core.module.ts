import {inject, InjectionToken, NgModule, Optional, SkipSelf} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LoggerModule, NgxLoggerLevel} from 'ngx-logger';
import {HttpClientModule} from '@angular/common/http';
import {CORE_SERVICES} from '@service/index';
import {API_URL} from '@shared/variables/api-url.variable';
import {authConfig} from '@app/auth.config';
import {OAuthModule} from 'angular-oauth2-oidc';
import {AuthModule, environment} from 'fox-design';
import {EffectsModule} from '@ngrx/effects';
import {effects} from '@app/store/effects';
import {StoreModule} from '@ngrx/store';
import {reducers} from '@app/store/reducers';
import {StoreRouterConnectingModule} from '@ngrx/router-store';
import {DOCUMENT} from '@angular/common';

const API_URL_PROVIDER = {
  provide: API_URL,
  useValue: environment.service.kweria3.url
};

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    LoggerModule.forRoot({
      level: NgxLoggerLevel.DEBUG,
      serverLogLevel: NgxLoggerLevel.OFF
    }),
    EffectsModule.forRoot(effects),
    StoreModule.forRoot(reducers),
    StoreRouterConnectingModule.forRoot(),
    OAuthModule.forRoot(),
    AuthModule.forRoot(authConfig)
  ],
  providers: [CORE_SERVICES, API_URL_PROVIDER]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule?: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }
}

export const WINDOW = new InjectionToken<Window>(
  'An abstraction over global window object',
  {
    factory: () => inject(DOCUMENT).defaultView
  },
);
