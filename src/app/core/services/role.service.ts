import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Store} from '@ngrx/store';
import {CoreState} from '@app/store/reducers';
import {selectUser, User} from 'fox-design';
import {map} from 'rxjs/operators';

@Injectable()
export class RoleService {

  user$: Observable<User>;

  constructor(private store: Store<CoreState>) {
    this.user$ = this.store.select(selectUser);
  }

  isManager(): Observable<boolean> {
    return this.user$.pipe(
      map((d) => {
        return d.login === '118881115';
      })
    );
  }

  isCompany(): Observable<boolean> {
    return this.user$.pipe(
      map((d) => {
        return false;
      })
    );
  }
}


