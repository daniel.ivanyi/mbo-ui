import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {selectUser, User} from 'fox-design';
import {Store} from '@ngrx/store';
import {CoreState} from '@app/store/reducers';
import {map} from 'rxjs/operators';

@Injectable()
export class UserService {

  user$: Observable<User>;

  constructor(private store: Store<CoreState>) {
    this.user$ = this.store.select(selectUser);
  }

  getDivision(): Observable<string> {
    return this.user$.pipe(
      map((d) => {
        return this.getDivisionByUser(d);
      })
    );
  }

  getDepartment(): Observable<string> {
    return this.user$.pipe(
      map((d) => {
        return this.getDepartmentByUser(d);
      })
    );
  }

  getDivisionByUser(user: User): string {
    const matches = user.name.match(/\((.*)\)/i);
    if (matches.length > 1) {
      return matches[1];
    }

    return null;
  }

  getDepartmentByUser(user: User): string {
    const department = this.getDivisionByUser(user);
    const matches = department.match(/([A-Z]{3})/);
    if (matches.length > 1) {
      return matches[1];
    }

    return null;
  }
}
