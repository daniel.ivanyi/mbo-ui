import {EvaluationType} from 'mbo-angular-client';

export class EvaluationTypeParser {
  public static texts = [
    {key: EvaluationType.Max, text: '+', description: 'čo najvyššie'},
    {key: EvaluationType.Min, text: '-', description: 'čo najnižšie'},
    {key: EvaluationType.Mark, text: 'Z', description: 'známka'},
    {key: EvaluationType.Ko, text: 'KO', description: 'KO'}
  ];

  static getText(type: EvaluationType): string {
    return this.texts.find((d) => d.key === type)?.text;
  }

  static getDescription(type: EvaluationType): string {
    return this.texts.find((d) => d.key === type)?.description;
  }
}
