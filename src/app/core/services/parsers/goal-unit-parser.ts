import {GoalUnitType} from 'mbo-angular-client';

export class GoalUnitParser {
  public static texts = [
    {key: GoalUnitType.MilEuro, text: 'mil. EUR'},
    {key: GoalUnitType.Clients, text: 'klientov'},
    {key: GoalUnitType.Pieces, text: 'ks'},
    {key: GoalUnitType.Percent, text: '%'}
  ];

  public static getText(unit: GoalUnitType): string {
    return this.texts.find((d) => d.key === unit)?.text;
  }
}

