import {GoalAssignmentType} from 'mbo-angular-client';

export class GoalAssignmentTypeParser {
  public static texts = [
    {key: GoalAssignmentType.Company, text: 'celofiremné'},
    {key: GoalAssignmentType.Department, text: 'úsekové'},
    {key: GoalAssignmentType.Group, text: 'týmove'},
    {key: GoalAssignmentType.Person, text: 'osobné'}
  ];

  public static options = [
    {key: GoalAssignmentType.Company, text: 'Celá firma'},
    {key: GoalAssignmentType.Department, text: 'Úsek'},
    {key: GoalAssignmentType.Group, text: 'Tým'},
    {key: GoalAssignmentType.Person, text: 'Osoba'}
  ];

  public static getText(type: GoalAssignmentType): string {
    return this.texts.find((d) => d.key === type)?.text;
  }

  public static getTitle(type: GoalAssignmentType): string {
    const planTypeText = GoalAssignmentTypeParser.getText(type);

    return `${planTypeText.charAt(0).toUpperCase()}${planTypeText.slice(1)}`;
  }
}
