import {EducationType} from '@data/education-type';

export class EducationTypeParser {
  public static texts = [
    {key: EducationType.PROFI, text: 'odborné'},
    {key: EducationType.SOFT, text: 'soft skills'},
    {key: EducationType.HARD, text: 'hard skills'},
    {key: EducationType.LANGUAGE, text: 'jazykové'},
    {key: EducationType.PROJECT, text: 'projektové'}
  ];

  public static getText(type: EducationType): string {
    return this.texts.find((d) => d.key === type)?.text;
  }
}
