export const DynamicAsideMenuConfig = {
  items: [
    {
      title: 'Ciele',
      root: true,
      bullet: 'dot',
      svg: './assets/media/svg/icons/Communication/Flag.svg',
      page: '/goals',
    },
    {
      title: 'MBO',
      root: true,
      bullet: 'dot',
      page: '/user',
      svg: './assets/media/svg/icons/General/User.svg'
    },
    {
      title: 'Hodnotenie',
      root: true,
      bullet: 'dot',
      page: '/evaluation',
      svg: './assets/media/svg/icons/Communication/Clipboard-list.svg'
    }
  ]
};
