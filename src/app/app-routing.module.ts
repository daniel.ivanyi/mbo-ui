import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from 'fox-design';
import {AboutComponent} from '@modules/about/about.component';

const routes: Routes = [
  {
    path: 'about',
    loadChildren: () => import('@modules/about/about.module').then(m => m.AboutModule),
    canLoad: [AuthGuard],
    canActivate: [AuthGuard]
  },
  {
    path: 'goals',
    loadChildren: () => import('@modules/goals/goals.module').then(m => m.GoalsModule),
    canLoad: [AuthGuard],
    canActivate: [AuthGuard]
  },
  {
    path: 'evaluation',
    loadChildren: () => import('@modules/evaluation/evaluation.module').then(m => m.EvaluationModule)
  },
  {
    path: 'user',
    loadChildren: () => import('@modules/mbo/mbo.module').then(m => m.MboModule)
  },
  {path: '', component: AboutComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {initialNavigation: 'disabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
