import {Pipe, PipeTransform} from '@angular/core';
import {Goal} from 'mbo-angular-client';
import {GoalAssignmentTypeParser} from '@service/parsers/goal-assignment-type-parser';

@Pipe({
  name: 'plan_title'
})
export class PlanTitlePipe implements PipeTransform {
  transform(value: Goal): any {
    if (!value) {
      return '';
    }

    const title = GoalAssignmentTypeParser.getTitle(value.assignmentType);
    const assignTo = value.assignTo ? ` (${value.assignTo})` : '';

    return `${title}${assignTo} ciele`;
  }
}
