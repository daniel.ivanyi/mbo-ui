import {Pipe, PipeTransform} from '@angular/core';
import {EvaluationTypeParser} from '@service/parsers/evaluation-type';
import {EvaluationType} from 'mbo-angular-client';

@Pipe({
  name: 'evaluation_type_desc'
})
export class EvaluationTypeDescPipe implements PipeTransform {

  transform(value: EvaluationType): any {
    if (!value) {
      return '';
    }

    return EvaluationTypeParser.getDescription(value);
  }
}
