import {Pipe, PipeTransform} from '@angular/core';
import {EducationType} from '../../data/education-type';
import {EducationTypeParser} from '@service/parsers/education-type-parser';

@Pipe({
  name: 'education_type'
})
export class EducationTypePipe implements PipeTransform {

  transform(value: EducationType): any {
    return EducationTypeParser.getText(value)
  }
}
