import {GoalUnitPipe} from '@shared/pipes/goal-unit.pipe';
import {EvaluationTypePipe} from '@shared/pipes/evaluation-type.pipe';
import {EvaluationTypeDescPipe} from '@shared/pipes/evaluation-type-desc.pipe';
import {PlanTitlePipe} from '@shared/pipes/plan-title.pipe';
import {SuccessBackgroundPipe} from '@shared/pipes/success-background.pipe';
import {GoalsWeightPerYearPipe} from '@shared/pipes/goals-weight-per-year.pipe';
import {MboSlovakPercentPipe} from '@shared/pipes/mbo-slovak-percent.pipe';
import {EducationTypePipe} from '@shared/pipes/education-type.pipe';

export const SHARED_PIPES = [
  GoalUnitPipe,
  EvaluationTypePipe,
  EvaluationTypeDescPipe,
  PlanTitlePipe,
  SuccessBackgroundPipe,
  GoalsWeightPerYearPipe,
  MboSlovakPercentPipe,
  EducationTypePipe
];
