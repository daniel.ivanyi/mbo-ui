import {Pipe, PipeTransform} from '@angular/core';
import {Goal} from 'mbo-angular-client';
import {GoalAssignmentTypeParser} from '@service/parsers/goal-assignment-type-parser';
import {MboSlovakPercentPipe} from '@shared/pipes/mbo-slovak-percent.pipe';

@Pipe({
  name: 'goals_weight_per_year'
})
export class GoalsWeightPerYearPipe implements PipeTransform {

  constructor(private percentPipe: MboSlovakPercentPipe) {
  }

  transform(goals: Goal[]): any {
    if (!goals || goals.length === 0) {
      console.log('no goals');
      return '';
    }

    const first = goals[0];
    const year = first.year;
    const assignmentType = GoalAssignmentTypeParser.getTitle(first.assignmentType);
    const assignTo = first.assignTo ? ` (${first.assignTo})` : '';
    const weight = this.percentPipe.transform(Number(goals
      .map((d) => d.weight)
      .reduce((sum, d) => sum + d, 0)
      .toFixed(2)));
    const targetRewardSum = goals
      .map((d) => d.targetReward)
      .reduce((sum, d) => sum + d, 0);
    const targetRewardAvg = (targetRewardSum / goals.length) || 0;
    const targetReward = this.percentPipe.transform(Number((targetRewardAvg).toFixed(2)));

    return `${assignmentType}${assignTo} ciele na rok ${year} majú celkovú váhu ${weight}, plnenie pre priznanie cieľovej odmeny je ${targetReward}.`;
  }
}
