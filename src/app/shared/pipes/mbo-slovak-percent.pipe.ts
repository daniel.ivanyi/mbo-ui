import {Pipe, PipeTransform} from '@angular/core';
import {PercentPipe} from '@angular/common';

@Pipe({
  name: 'slovak_percent'
})
export class MboSlovakPercentPipe implements PipeTransform {

  constructor(private percent: PercentPipe) {
  }

  transform(value: number | undefined): any {
    if (!value) {
      value = 0.00;
    }

    return this.percent.transform(value, '0.2', 'sk');
  }
}
