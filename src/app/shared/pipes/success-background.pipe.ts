import {Pipe, PipeTransform} from '@angular/core';
import {Goal, GoalEvaluation} from 'mbo-angular-client';
import {EvaluationIntervalProvider} from '@data/evaluation-interval.provider';

@Pipe({
  name: 'success_background'
})
export class SuccessBackgroundPipe implements PipeTransform {

  transform(goal: Goal): any {
    let evaluation = goal.semiAnnualEvaluation;
    if (goal.annualEvaluation) {
      evaluation = goal.annualEvaluation;
    }
    if (!evaluation || !evaluation.completed) {
      return '';
    }

    const completed = evaluation.completed;

    const interval = EvaluationIntervalProvider.getIntervals()
      .find((d) => d.start < completed && completed < (d.end ?? 100));

    return interval?.background;
  }
}
