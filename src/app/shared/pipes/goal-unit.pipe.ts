import {Pipe, PipeTransform} from '@angular/core';
import {GoalUnitParser} from '@service/parsers/goal-unit-parser';
import {GoalUnitType} from 'mbo-angular-client';

@Pipe({
  name: 'goal_unit_text'
})
export class GoalUnitPipe implements PipeTransform {

  transform(value: GoalUnitType): any {
    if (!value) {
      return '';
    }

    return GoalUnitParser.getText(value);
  }
}
