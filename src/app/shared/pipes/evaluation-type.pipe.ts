import {Pipe, PipeTransform} from '@angular/core';
import {EvaluationTypeParser} from '@service/parsers/evaluation-type';
import {EvaluationType} from 'mbo-angular-client';

@Pipe({
  name: 'evaluation_type_text'
})
export class EvaluationTypePipe implements PipeTransform {
  transform(value: EvaluationType): any {
    if (!value) {
      return '';
    }

    return EvaluationTypeParser.getText(value);
  }
}
