import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {faCheck, faCogs, faEdit, faLock, faPlus, faTrashAlt, faUnlock} from '@fortawesome/free-solid-svg-icons';
import {ConfirmDialogComponent, DialogService, FoxMessageType, selectUser, showMessage, User} from 'fox-design';
import {GoalDetailComponent} from '@shared/components/goal-detail/goal-detail.component';
import {map, tap} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {EvaluationType, FindGoalCriteria, Goal, GoalEvaluation} from 'mbo-angular-client';
import {Store} from '@ngrx/store';
import {RoleService} from '@service/role.service';
import {EvaluationIntervalProvider} from '@data/evaluation-interval.provider';
import * as MboSelectors from '@app/store/selectors/mbo.selectors';
import * as MboActions from '@app/store/actions/mbo.actions';
import {CommonTexts} from '@data/common-texts';
import {GoalAssignmentTypeParser} from '@service/parsers/goal-assignment-type-parser';

@Component({
  selector: 'app-goal-list',
  templateUrl: './goal-list.component.html'
})
export class GoalListComponent implements OnInit {

  @Input() set goalInput(goals: Goal[]) {
    console.log('input goals', goals);
    this.goals = goals;
  }

  @Input() set criteriaInput(criteria: FindGoalCriteria) {
    console.log('input criteria', criteria);
    this.criteria = criteria;
  }

  @Input() actions = true;

  @Output() goalsChanged: EventEmitter<Goal[]> = new EventEmitter<Goal[]>();

  goals$!: Observable<Goal[]>;
  goals: Goal[];
  criteria$!: Observable<FindGoalCriteria>;
  criteria: FindGoalCriteria;
  targetReward = 0;

  deleteIcon = faTrashAlt;
  editIcon = faEdit;
  plusIcon = faPlus;
  cogsIcon = faCogs;
  lockIcon = faLock;
  unLockIcon = faUnlock;
  okIcon = faCheck;

  detailDialog$: Observable<any>;
  confirmedRemoveGoalDialog$: Observable<boolean>;
  confirmedLock$: Observable<boolean>;
  confirmedUnlock$: Observable<boolean>;
  isManager$: Observable<boolean>;
  isManager = false;
  isCompany$: Observable<boolean>;
  isCompany = false;
  user$: Observable<User>;
  user: User;

  isNew = 'NEW';
  assignmentTypeText: string;

  constructor(private fb: FormBuilder,
              private store: Store,
              private dialogService: DialogService,
              private roleService: RoleService) {
  }

  ngOnInit(): void {
    this.initUser();
    this.initIsManager();
    this.initIsCompany();

    if (!this.goals) {
      this.initGoalsListener();
    }
    if (!this.criteria) {
      this.initCriteriaListener();
    }
  }

  openGoal(index: string | number, goal: Goal): void {
    const modalRef = this.dialogService.open(GoalDetailComponent, {
      formGroup: this.getGoalAsFormGroup(goal),
      store: this.store
    });
    this.detailDialog$ = modalRef.closed.pipe(
      tap(d => {
        if (d) {
          this.mergeChanges(index, d);
        }
      }));
  }

  openConfirmRemoveGoalDialog(index: number): void {
    const goalToRemove = this.goals[index];
    const modal = this.dialogService.open(ConfirmDialogComponent,
      {question: `Vymazať zvolený ciel - ${goalToRemove.name}?`});

    this.confirmedRemoveGoalDialog$ = modal.closed.pipe(
      tap((d) => {
        if (d) {
          this.removeGoal(index);
        }
      })
    );
  }

  openConfirmLockGoalDialog(index: number): void {
    const goalToLock = this.goals[index];
    const modal = this.dialogService.open(ConfirmDialogComponent,
      {question: `Uzamknúť zvolený ciel - ${goalToLock.name}?`});

    this.confirmedLock$ = modal.closed.pipe(
      tap((d) => {
        if (d) {
          this.lockGoal(index);
        }
      })
    );
  }

  openConfirmUnLockGoalDialog(index: number): void {
    const goalToLock = this.goals[index];
    const modal = this.dialogService.open(ConfirmDialogComponent,
      {question: `Odomknúť zvolený ciel - ${goalToLock.name}?`});

    this.confirmedLock$ = modal.closed.pipe(
      tap((d) => {
        if (d) {
          this.unLockGoal(index);
        }
      })
    );
  }

  initAsPreviousYear(): void {
    this.store.dispatch(MboActions.initGoal());
  }

  save(goal: Goal): void {
    console.log('save', goal);
    this.store.dispatch(MboActions.saveGoal({goal}));
  }

  onSemiAnnualEvaluationChanged(index: number, goal: Goal, $event: GoalEvaluation): void {
    goal = {
      ...goal,
      semiAnnualEvaluation: $event
    };

    if (goal.semiAnnualEvaluation?.reality) {
      goal = {...this.evaluateGoal(goal)};
    }

    this.goals = [...this.goals];
    this.goals[index] = goal;
    this.emitGoalsChanged();
  }

  onAnnualEvaluationChanged(index: number, goal: Goal, $event: GoalEvaluation): void {
    goal = {
      ...goal,
      annualEvaluation: $event
    };

    if (goal.annualEvaluation?.reality) {
      goal = {...this.evaluateGoal(goal)};
    }

    this.goals = [...this.goals];
    this.goals[index] = goal;
    this.emitGoalsChanged();
  }

  private initUser(): void {
    this.user$ = this.store.select(selectUser).pipe(
      tap((d) => {
        this.user = d;
      })
    );
  }

  private initIsManager(): void {
    this.isManager$ = this.roleService.isManager().pipe(
      tap((d) => {
        this.isManager = d;
      })
    );
  }

  private initIsCompany(): void {
    this.isCompany$ = this.roleService.isCompany().pipe(
      tap((d) => {
        this.isCompany = d;
      })
    );
  }

  private initGoalsListener(): void {
    this.goals$ = this.store.select(MboSelectors.goals).pipe(
      map((d) => {
        if (d) {
          this.goals = [...d];
          return d;
        }
      })
    );
  }

  private initCriteriaListener(): void {
    this.criteria$ = this.store.select(MboSelectors.goalCriteria).pipe(
      map((d) => {
        if (d) {
          this.criteria = d;
          const assignment = GoalAssignmentTypeParser.getText(d.assignmentType);
          const assignTo = d.assignTo ? ` (${d.assignTo})` : '';
          this.assignmentTypeText = `${assignment}${assignTo} ciele`;
          return d;
        }
      })
    );
  }

  private getGoalAsFormGroup(goal: Goal): FormGroup {
    const fg = this.fb.group({
      id: [null],
      year: [null, Validators.required],
      name: [null, Validators.required],
      description: [null],
      weight: [null, [Validators.required, Validators.min(1), Validators.max(100)]],
      value: [null, Validators.required],
      unit: [null, Validators.required],
      evaluationType: [null, Validators.required],
      assignmentType: [null, Validators.required],
      assignTo: [null]
    });
    setTimeout(() => {
      fg.patchValue({
        ...goal,
        year: this.criteria?.year,
        assignmentType: this.criteria?.assignmentType,
        assignTo: this.criteria?.assignTo,
        weight: goal ? (goal.weight * 100) : null
      });
    });

    return fg;
  }

  private mergeChanges(index: string | number, goal: Goal): void {
    if (goal?.semiAnnualEvaluation?.reality || goal?.annualEvaluation?.reality) {
      goal = {...this.evaluateGoal(goal)};
    }

    this.goals = [...this.goals];

    // vaha naspat na percentualny bod
    goal.weight = goal.weight / 100;
    if (index !== this.isNew) {
      goal = {
        ...this.goals[index],
        ...goal
      };
      this.goals[index] = goal;
    } else {
      this.goals.push(goal);
    }
    this.save(goal);
    this.emitGoalsChanged();
  }

  private removeGoal(index: number): void {
    const goalIdToRemove = this.goals[index].id;
    this.goals = [...this.goals];
    this.goals.splice(index, 1);
    this.store.dispatch(MboActions.removeGoal({goalId: goalIdToRemove}));

    this.emitGoalsChanged();
  }

  private getSumOfGoalWeights(goals: Goal[]): number {
    if (!goals || goals.length === 0) {
      return 0.00;
    }
    return Number(goals
      .map((d) => d.weight)
      .reduce((sum, d) => sum + d, 0)
      .toFixed(2));
  }

  private evaluateGoal(goal: Goal): Goal {
    if (!goal?.semiAnnualEvaluation?.reality && !goal?.annualEvaluation?.reality) {
      console.log('no reality of goal, cannot evaluate it');
      return;
    }
    let reality: number;
    let completed: number;
    let targetReward: number;
    if (goal.semiAnnualEvaluation) {
      reality = goal?.semiAnnualEvaluation?.reality?.evaluation;
      completed = this.getGoalSuccess(reality, goal);
      targetReward = this.getTargetReward(completed);
      goal = {
        ...goal,
        semiAnnualEvaluation: {
          ...goal.semiAnnualEvaluation,
          completed
        },
        targetReward
      };
    }
    if (goal.annualEvaluation) {
      reality = goal?.annualEvaluation?.reality?.evaluation;
      completed = this.getGoalSuccess(reality, goal);
      targetReward = this.getTargetReward(completed);
      goal = {
        ...goal,
        annualEvaluation: {
          ...goal.annualEvaluation,
          completed
        },
        targetReward
      };
    }

    this.save(goal);

    return goal;
  }

  private getGoalSuccess(reality: number, goal: Goal): number {
    if (EvaluationType.Max === goal.evaluationType) {
      return Number((reality / goal.value).toFixed(3));
    } else if (EvaluationType.Min === goal.evaluationType) {
      return 1 - Number(((reality - goal.value) / goal.value).toFixed(3));
    } else if ([EvaluationType.Mark, EvaluationType.Ko].includes(goal.evaluationType)) {
      return EvaluationIntervalProvider.getIntervals()
        .find((d) => d.mark === reality)?.successOfGoal;
    }
  }

  private getTargetReward(success: number): number {
    const targetReward = EvaluationIntervalProvider.getIntervals()
      .find((d) => d.start <= success && success <= (d.end ?? 100))?.successOfGoal;

    return targetReward;
  }

  private getSumOfGoalsCompletion(goals: Goal[]): number {
    if (!goals || goals.length === 0) {
      return 0.00;
    }

    this.targetReward = Number(goals
      .map((d) => d?.targetReward)
      .reduce((sum, d) => sum + d, 0)
      .toFixed(4));

    return this.targetReward;
  }

  private lockGoal(index: number): void {
    let goal = this.goals[index];
    goal = {
      ...goal,
      lock: {
        username: this.user.name,
        dateOfLock: CommonTexts.now
      }
    };
    this.goals[index] = goal;
    this.emitGoalsChanged();
    this.store.dispatch(MboActions.lockGoal({goal}));
  }

  private unLockGoal(index: number): void {
    let goal = this.goals[index];
    goal = {
      ...goal,
      lock: null
    };
    this.goals[index] = goal;
    this.emitGoalsChanged();
    this.store.dispatch(MboActions.unLockGoal({goal}));
  }

  private emitGoalsChanged(): void {
    this.goalsChanged.emit(this.goals);
  }
}
