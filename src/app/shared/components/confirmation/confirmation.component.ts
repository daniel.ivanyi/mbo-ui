import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {faCheck} from '@fortawesome/free-solid-svg-icons';
import {Signature} from 'mbo-angular-client';
import {ConfirmDialogComponent, DialogService, User} from 'fox-design';
import {tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {UserService} from '@service/user.service';
import {CommonTexts} from '@data/common-texts';
import {RoleService} from '@service/role.service';

@Component({
  selector: 'app-confirmation',
  templateUrl: 'confirmation.component.html'
})
export class ConfirmationComponent implements OnInit {

  @Input() signature: Signature;
  @Input() isManager: boolean;
  @Input() question!: string;
  @Input() user!: User;
  @Output() signatureChanged: EventEmitter<Signature> = new EventEmitter<Signature>();

  iconCheck = faCheck;

  confirmedSettings$: Observable<any>;
  isManager$: Observable<boolean>;

  constructor(private dialogService: DialogService,
              private userService: UserService,
              private roleService: RoleService) {
  }

  ngOnInit(): void {
    this.isManager$ = this.roleService.isManager().pipe();
  }

  openSettingConfirmation(): void {
    const modal = this.dialogService.open(ConfirmDialogComponent,
      {question: this.question});

    this.confirmedSettings$ = modal.closed.pipe(
      tap((d) => {
        if (d) {
          this.confirmSetting();
        }
      })
    );
  }

  private confirmSetting(): void {
    const confirmed = {
      dateOfSignature: CommonTexts.now,
      person: {
        username: this.user.login,
        name: this.user.name
      }
    };

    this.signatureChanged.emit(confirmed);
  }
}
