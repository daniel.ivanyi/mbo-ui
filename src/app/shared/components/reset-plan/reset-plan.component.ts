import {Component} from '@angular/core';
import {faCog} from '@fortawesome/free-solid-svg-icons';
import {Store} from '@ngrx/store';
import {ConfirmDialogComponent, DialogService} from 'fox-design';
import {tap} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-reset-plan',
  templateUrl: './reset-plan.component.html'
})
export class ResetPlanComponent {

  cogIcon = faCog;
  confirmedResetDialog$: Observable<boolean>;

  constructor(private store: Store,
              private dialogService: DialogService) {
  }

  openConfirmResetDialog(): void {
    const modal = this.dialogService.open(ConfirmDialogComponent,
      {question: 'Naozaj vymazať všetky ciele?'}
    );
    this.confirmedResetDialog$ = modal.closed.pipe(
      tap((d) => {
        if (d) {
          this.reset();
        }
      })
    );
  }

  reset(): void {
    console.log('reset');
  }
}
