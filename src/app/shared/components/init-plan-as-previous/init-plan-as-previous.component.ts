import {Component, Input} from '@angular/core';
import {faCopy} from '@fortawesome/free-solid-svg-icons';
import {Store} from '@ngrx/store';
import * as MboActions from '../../../store/actions/mbo.actions';

@Component({
  selector: 'app-init-plan-as-previous',
  templateUrl: 'init-plan-as-previous.component.html'
})
export class InitPlanAsPreviousComponent {

  copyIcon = faCopy;

  constructor(private store: Store) {
  }

  loadPreviousYear(): void {
  }
}
