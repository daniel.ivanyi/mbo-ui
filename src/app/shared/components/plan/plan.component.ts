import {Component, Input, OnInit} from '@angular/core';
import {DialogService} from 'fox-design';
import {Store} from '@ngrx/store';
import {tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {faCheck} from '@fortawesome/free-solid-svg-icons';
import {DateTime} from 'luxon';
import {Goal, GoalAssignmentType} from 'mbo-angular-client';
import {UserService} from '@service/user.service';
import {RoleService} from '@service/role.service';

@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html'
})
export class PlanComponent implements OnInit {
  @Input() typeOfPlan!: GoalAssignmentType;

  goals: Goal[];

  title: string;
  division$: Observable<string>;
  division: string;

  isManager$: Observable<boolean>;
  isManager = false;
  isCompany$: Observable<boolean>;
  isCompany = false;

  okIcon = faCheck;

  readonly thisYear = DateTime.now().year;

  constructor(private dialogService: DialogService,
              private store: Store,
              private userService: UserService,
              private roleService: RoleService) {
  }

  ngOnInit(): void {
    console.log('init');

    this.division$ = this.userService.getDivision().pipe(
      tap((d) => {
        if (d) {
          this.division = d;
        }
      })
    );
    this.isManager$ = this.roleService.isManager().pipe(
      tap((d) => {
        this.isManager = d;
      })
    );
    this.isCompany$ = this.roleService.isCompany().pipe(
      tap((d) => {
        this.isCompany = d;
      })
    );
  }

  savePlan(): void {
    console.log('savePlan');
  }
}
