import {Component, Input} from '@angular/core';
import {EvaluationType, GoalEvaluationItem} from 'mbo-angular-client';
import {FormGroup} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {AbstractDialogComponent, FormElementOption} from 'fox-design';
import {Store} from '@ngrx/store';

@Component({
  templateUrl: 'goal-evaluation-dialog.component.html'
})
export class GoalEvaluationDialogComponent extends AbstractDialogComponent {

  @Input() formGroup!: FormGroup;
  @Input() evaluationType: EvaluationType;
  @Input() store: Store;
  marks: FormElementOption<number>[] = [
    {value: 1, name: '1'},
    {value: 2, name: '2'},
    {value: 3, name: '3'},
    {value: 4, name: '4'},
    {value: 5, name: '5'},
    {value: 6, name: '6'},
  ];
  kos: FormElementOption<number>[] = [
    {value: 4, name: 'Splnene'},
    {value: 1, name: 'Neplnene'},
  ];

  constructor(activeModal: NgbActiveModal) {
    super(activeModal);
  }
  onSubmit(): void {
    if (this.formGroup.valid) {
      const evaluation = {...this.formGroup.getRawValue()} as GoalEvaluationItem;
      console.log('close evaluation', evaluation);
      this.activeModal.close(evaluation);
    } else {
      this.formGroup.markAllAsTouched();
      console.log('formGroup is not valid', this.formGroup);
    }
  }

  dismiss(): void {
    this.activeModal.dismiss();
  }
}
