import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Goal, GoalEvaluationItem} from 'mbo-angular-client';
import {faBan, faCheck, faEdit, faThumbsUp} from '@fortawesome/free-solid-svg-icons';
import {DialogService, selectUser, User} from 'fox-design';
import {map, tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {
  GoalEvaluationDialogComponent
} from '@shared/components/goal-evaluation/goal-evaluation-item/goal-evaluation-dialog.component';
import {FormBuilder, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';
import {CommonTexts} from '@data/common-texts';

@Component({
  selector: 'app-goal-evaluation-item',
  templateUrl: 'goal-evaluation-item.component.html'
})
export class GoalEvaluationItemComponent implements OnInit {

  @Input() item!: GoalEvaluationItem;
  @Input() goal!: Goal;
  @Input() canUpdate;
  @Output() itemChanged: EventEmitter<GoalEvaluationItem> = new EventEmitter<GoalEvaluationItem>();

  evaluateIcon = faThumbsUp;
  editIcon = faEdit;
  checkIcon = faCheck;
  noCheckIcon = faBan;
  evaluationDialog$: Observable<any>;

  user$: Observable<User>;
  user: User;

  constructor(private fb: FormBuilder,
              private store: Store,
              private dialogService: DialogService) {
  }

  ngOnInit(): void {
    this.initUserListener();
  }

  openEvaluationDialog(): void {
    let dialogSize = 'sm';
    if (this.goal?.evaluationType === 'MARK' || this.goal?.evaluationType === 'KO') {
      dialogSize = '';
    }
    const modalRef = this.dialogService.open(GoalEvaluationDialogComponent, {
      formGroup: this.fb.group({
        evaluation: [this.item?.evaluation, Validators.required]
      }),
      evaluationType: this.goal.evaluationType,
      store: this.store
    }, {
      size: dialogSize
    });
    this.evaluationDialog$ = modalRef.closed.pipe(
      tap((d) => {
        if (d) {
          this.item = {
            ...d,
            person: {
              username: this.user.login,
              name: this.user.name
            },
            date: CommonTexts.now
          };
          console.log('close evaluation', this.item);

          this.itemChanged.emit(this.item);
        }
      }));
  }

  private initUserListener(): void {
    this.user$ = this.store.select(selectUser).pipe(
      map((d) => {
        this.user = d;
        return d;
      })
    );
  }
}
