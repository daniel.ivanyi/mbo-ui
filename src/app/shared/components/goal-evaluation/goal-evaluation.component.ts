import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Goal, GoalEvaluation, GoalEvaluationItem} from 'mbo-angular-client';
import {faStar, faUser} from '@fortawesome/free-solid-svg-icons';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {RoleService} from '@service/role.service';

@Component({
  selector: 'app-goal-evaluation',
  templateUrl: 'goal-evaluation.component.html'
})
export class GoalEvaluationComponent implements OnInit {

  @Input() evaluation!: GoalEvaluation;
  @Input() goal!: Goal;
  @Output() evaluationChanged: EventEmitter<GoalEvaluation> = new EventEmitter<GoalEvaluation>();

  userIcon = faUser;
  managerIcon = faStar;
  isManager$: Observable<boolean>;
  isManager = false;

  constructor(private roleService: RoleService) {
  }

  ngOnInit(): void {
    this.initIsManagerListener();
  }

  onUserEvaluationChanged($event: GoalEvaluationItem): void {
    this.evaluation = {
      ...this.evaluation,
      userEvaluation: {...$event}
    };
    this.evaluationChanged.emit(this.evaluation);
  }

  onManagerEvaluationChanged($event: GoalEvaluationItem): void {
    this.evaluation = {
      ...this.evaluation,
      managerEvaluation: {...$event}
    };
    this.evaluationChanged.emit(this.evaluation);
  }

  onRealityChanged($event: GoalEvaluationItem): void {
    this.evaluation = {
      ...this.evaluation,
      reality: {...$event}
    };
    this.evaluationChanged.emit(this.evaluation);
  }

  private initIsManagerListener(): void {
    this.isManager$ = this.roleService.isManager().pipe(
      tap((d) => {
        this.isManager = d;
      })
    );
  }
}
