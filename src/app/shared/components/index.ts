import {GoalListComponent} from '@shared/components/goal-list/goal-list.component';
import {GoalDetailComponent} from '@shared/components/goal-detail/goal-detail.component';
import {PlanDefinitionsComponent} from '@shared/components/plan-definitions/plan-definitions.component';
import {InitPlanAsPreviousComponent} from '@shared/components/init-plan-as-previous/init-plan-as-previous.component';
import {ResetPlanComponent} from '@shared/components/reset-plan/reset-plan.component';
import {PlanComponent} from '@shared/components/plan/plan.component';
import {ConfirmationComponent} from '@shared/components/confirmation/confirmation.component';
import {GoalEvaluationComponent} from '@shared/components/goal-evaluation/goal-evaluation.component';
import {
  GoalEvaluationItemComponent
} from '@shared/components/goal-evaluation/goal-evaluation-item/goal-evaluation-item.component';
import {
  GoalEvaluationDialogComponent
} from '@shared/components/goal-evaluation/goal-evaluation-item/goal-evaluation-dialog.component';

export const SHARED_COMPONENTS = [
  PlanComponent,
  GoalListComponent,
  GoalDetailComponent,
  GoalEvaluationComponent,
  GoalEvaluationItemComponent,
  GoalEvaluationDialogComponent,
  PlanDefinitionsComponent,
  InitPlanAsPreviousComponent,
  ResetPlanComponent,
  ConfirmationComponent
];
