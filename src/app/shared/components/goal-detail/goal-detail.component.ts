import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {AbstractDialogComponent, FormElementOption} from 'fox-design';
import {GoalUnitParser as GoalUnitParser} from '@service/parsers/goal-unit-parser';
import {EvaluationTypeParser as EvaluationTypeParse} from '@service/parsers/evaluation-type';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Store} from '@ngrx/store';
import * as EvaluationActions from '../../../store/actions/evaluation.ations';
import * as EvaluationSelectors from '../../../store/selectors/evaluation.selectors';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {EvaluationType, Goal, GoalUnitType} from 'mbo-angular-client';

@Component({
  selector: 'app-goal-detail',
  templateUrl: './goal-detail.component.html'
})
export class GoalDetailComponent extends AbstractDialogComponent implements OnInit {
  @Input() formGroup!: FormGroup;
  @Input() store!: Store;

  evaluationTypeMark = EvaluationType.Mark;

  units: FormElementOption<GoalUnitType>[];
  evaluationTypes: FormElementOption<EvaluationType>[];
  markOptions$: Observable<FormElementOption<number>[]>;

  constructor(activeModal: NgbActiveModal) {
    super(activeModal);
  }

  ngOnInit(): void {
    console.log('goal form group', this.formGroup?.value);

    this.units = GoalUnitParser.texts.map((d) => {
      return {
        value: d.key,
        name: d.text
      } as FormElementOption<GoalUnitType>;
    });

    this.evaluationTypes = EvaluationTypeParse.texts
      .map((d) => {
        return {
          value: d.key,
          name: d.description
        } as FormElementOption<EvaluationType>;
      });

    this.store.dispatch(EvaluationActions.load());
    this.markOptions$ = this.store.select(EvaluationSelectors.intervals).pipe(
      map((d) => {
        if (!d) {
          return [] as FormElementOption<number>[];
        }

        return d.map((a) => {
          return {value: a.mark, name: `${a.mark}`} as FormElementOption<number>;
        });
      })
    );
  }

  dismiss(): void {
    this.activeModal.dismiss();
  }

  onSubmit(): void {
    if (this.formGroup.valid) {
      const goal = {...this.formGroup.getRawValue()} as Goal;
      this.activeModal.close(goal);
    } else {
      this.formGroup.markAllAsTouched();
      console.log('goal formGroup is not valid', this.formGroup);
    }
  }
}
