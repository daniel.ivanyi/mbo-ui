import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-plan-definition',
  templateUrl: './plan-definitions.component.html'
})
export class PlanDefinitionsComponent implements OnInit {

  formGroup: FormGroup;

  constructor(private fb: FormBuilder) {
    this.formGroup = this.fb.group({
      year: [null, Validators.required],
      weight: [null, [Validators.required, Validators.min(0), Validators.max(100)]],
      completedOn: []
    });
  }

  ngOnInit(): void {
  }
}
