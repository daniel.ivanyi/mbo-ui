import {NgModule} from '@angular/core';
import {CommonModule, DecimalPipe, PercentPipe} from '@angular/common';
import {SHARED_PIPES} from './pipes';
import {SHARED_COMPONENTS} from './components';
import {FoxDesignModule} from 'fox-design/src/lib/common-components';
import {ReactiveFormsModule} from '@angular/forms';
import {AwesomeIconsModule, InterceptorsModule} from 'fox-design';
import {UserService} from '@service/user.service';


@NgModule({
  declarations: [
    ...SHARED_PIPES,
    ...SHARED_COMPONENTS
  ],
  exports: [
    ...SHARED_PIPES,
    ...SHARED_COMPONENTS,
    FoxDesignModule,
    InterceptorsModule
  ],
  providers: [
    ...SHARED_PIPES,
    UserService,
    PercentPipe,
    DecimalPipe
  ],
    imports: [
        CommonModule,
        FoxDesignModule,
        ReactiveFormsModule,
        AwesomeIconsModule
    ]
})
export class SharedModule {
}
