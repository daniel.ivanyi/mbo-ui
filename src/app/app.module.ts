import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CoreModule} from '@core/core.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {SharedModule} from '@shared/shared.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {CommonModule} from '@angular/common';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {
  AuthEffects,
  authFeatureKey,
  AuthGuard,
  AuthModule,
  AwesomeIconsModule,
  ConfigEnvironmentEffects,
  environment,
  getServiceUrl,
  LayoutModule,
  MessageEffects,
  MetronicLibModule,
  NavigationEffects,
  USER_FEATURE_REDUCER_TOKEN
} from 'fox-design';
import {DynamicAsideMenuConfig} from '@app/config/dynamic-aside-menu.config';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {LayoutConfig} from '@app/config/layout.config';
import {ApiModule as MboApiModule, Configuration as MboApiConfiguration} from 'mbo-angular-client';

export function mboApiConfigFactory(): MboApiConfiguration {
  const serviceUrl = getServiceUrl('/mbo/api', environment.service.report?.url);
  console.log(`mbo service url[${serviceUrl}]`);
  return new MboApiConfiguration({
    basePath: serviceUrl
  });
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CoreModule,
    AppRoutingModule,
    SharedModule,
    NgbModule,
    FlexLayoutModule,
    CommonModule,
    HttpClientModule,
    AwesomeIconsModule,
    StoreModule.forFeature(authFeatureKey, USER_FEATURE_REDUCER_TOKEN),
    EffectsModule.forFeature([AuthEffects, ConfigEnvironmentEffects, NavigationEffects, MessageEffects]),
    StoreDevtoolsModule.instrument({
      maxAge: 100, // Retains last 25 states
      logOnly: environment.param.environment === 'prod', // Restrict extension to log-only mode
    }),
    AuthModule,
    MetronicLibModule,
    LayoutModule.forRoot(DynamicAsideMenuConfig, null, LayoutConfig),
    MboApiModule.forRoot(mboApiConfigFactory)
  ],
  bootstrap: [
    AppComponent
  ],
  providers: [
    AuthGuard
  ]
})
export class AppModule {
}
