import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {AppModule} from '@app/app.module';
import {environment, environmentLoader} from 'fox-design';
import {take} from 'rxjs/operators';

environmentLoader.pipe(take(1)).subscribe((env: any) => {
  console.log('env', env);
  environment.service = env.service;
  environment.param = env.param;
  console.log('API Urls: ' + JSON.stringify(environment.service));

  platformBrowserDynamic().bootstrapModule(AppModule)
    .catch(err => console.log(err));

});
