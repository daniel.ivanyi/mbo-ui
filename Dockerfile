FROM registry.pss.sk/docker/nginx-health:latest
COPY dist /usr/share/nginx/html
COPY nginx-cors.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
